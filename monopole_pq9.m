close all; 
clear; 
clc;

physical_constants;
unit = 1e-3; # millimeters

f_start = 200e6;
f0 = 400e6; # center frequency
f_stop = 600e6;
lambda = c0 / f0 / unit; # wavelength in mm
monopole.length = 245;
monopole.radius = 1;
ground = floor(lambda / 14.7);


% feeding
feed.heigth = 3;
feed.R = 50;    %feed impedance


% size of the simulation box
SimBox = [1 1 1]*2*lambda;


%% setup FDTD parameter & excitation function
FDTD = InitFDTD( );
FDTD = SetGaussExcite( FDTD, f0, f0/2 );
BC = {'MUR' 'MUR' 'MUR' 'MUR' 'MUR' 'PML_8'}; % boundary conditions
FDTD = SetBoundaryCond( FDTD, BC );


%% setup CSXCAD geometry & mesh
max_res = floor(c0 / f0/ unit / 20); % cell size: lambda/20
CSX = InitCSX();


% create helix mesh
mesh.x = SmoothMeshLines([-monopole.radius 0 monopole.radius], 0.5);
% add the air-box
mesh.x = [mesh.x -SimBox(1)/2-ground  SimBox(1)/2+ground];
% create a smooth mesh between specified fixed mesh lines
mesh.x = SmoothMeshLines( mesh.x, max_res, 1.4);

% copy x-mesh to y-direction
mesh.y = mesh.x;


% create helix mesh in z-direction
mesh.z = SmoothMeshLines([0 feed.heigth monopole.length],1.5);
% add the air-box
mesh.z = unique([mesh.z -SimBox(3)/2 max(mesh.z)+SimBox(3)/2 ]);
% create a smooth mesh between specified fixed mesh lines
mesh.z = SmoothMeshLines( mesh.z, max_res, 1.4 );

CSX = DefineRectGrid( CSX, unit, mesh );


#monopole length
start = [0 0 feed.heigth];
stop =  [0 0 monopole.length];
CSX = AddMetal( CSX, 'monopole');
CSX = AddCylinder( CSX, 'monopole', 1, start, stop, monopole.radius);


# GND plane for monopole
start = [-ground/2 -ground/2 0];
stop  = [ground/2 ground/2 0.5]; 
CSX = AddMetal( CSX, 'GND' );
CSX = AddBox( CSX, 'GND', 1,  start, stop);


start = [0 0 0.5];
stop =  [0 0 feed.heigth];
[CSX port] = AddLumpedPort(CSX, 5, 1, feed.R, start, stop, [0 0 1], true);


%%nf2ff calc
start = [mesh.x(11)      mesh.y(11)     mesh.z(11)];
stop  = [mesh.x(end-10) mesh.y(end-10) mesh.z(end-10)];
[CSX nf2ff] = CreateNF2FFBox(CSX, 'nf2ff', start, stop, 'OptResolution', lambda/15);


# simulation
Sim_Path = 'tmp_arian';
Sim_CSX = 'arian.xml';
try confirm_recursive_rmdir(false, 'local'); end
[status, message, messageid] = rmdir( Sim_Path, 's' ); % clear previous
[status, message, messageid] = mkdir( Sim_Path ); % create empty folder
WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX ); % save file
CSXGeomPlot( [Sim_Path '/' Sim_CSX] ); % display geometry
RunOpenEMS( Sim_Path, Sim_CSX, ' -vvv');


# calculate Zin
freq = linspace( f_start, f_stop, 501 );
port = calcPort(port, Sim_Path, freq);
Zin = port.uf.tot ./ port.if.tot;
s11 = port.uf.ref ./ port.uf.inc;
P_in = 0.5 * port.uf.inc .* conj( port.if.inc ); % antenna feed power


%% Smith chart port reflection
%plotRefl(port, 'threshold', -10)
%title( 'reflection coefficient' );


% plot feed point impedance
figure
plot( freq/1e6, real(Zin), 'k-', 'Linewidth', 2 );
hold on
grid on
plot( freq/1e6, imag(Zin), 'r--', 'Linewidth', 2 );
title( 'feed point impedance' );
xlabel( 'frequency f / MHz' );
ylabel( 'impedance Z_{in} / Ohm' );
legend( 'real', 'imag' );


% plot reflection coefficient S11
figure
plot( freq/1e6, 20*log10(abs(s11)), 'k-', 'Linewidth', 2 );
grid on
title( 'reflection coefficient S_{11}' );
xlabel( 'frequency f / MHz' );
ylabel( 'reflection coefficient |S_{11}|' );
 
drawnow

%% NFFF contour plots %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%find resonance frequency from s11
f_res_ind = find(s11==min(s11));
f_res = freq(f_res_ind);

 
% calculate the far field at phi=0 degrees and at phi=90 degrees
disp( 'calculating far field at phi=[0 90] deg...' );

 
nf2ff = CalcNF2FF(nf2ff, Sim_Path, f_res, [-180:2:180]*pi/180, [0 90]*pi/180);

 
% display power and directivity
disp( ['radiated power: Prad = ' num2str(nf2ff.Prad) ' Watt']);
disp( ['directivity: Dmax = ' num2str(nf2ff.Dmax) ' (' num2str(10*log10(nf2ff.Dmax)) ' dBi)'] );
disp( ['efficiency: nu_rad = ' num2str(100*nf2ff.Prad./real(P_in(f_res_ind))) ' %']);

 
% normalized directivity as polar plot
figure
polarFF(nf2ff,'xaxis','theta','param',[1 2],'normalize',1)


% log-scale directivity plot
figure
plotFFdB(nf2ff,'xaxis','theta','param',[1 2])
% conventional plot approach
% plot( nf2ff.theta*180/pi, 20*log10(nf2ff.E_norm{1}/max(nf2ff.E_norm{1}(:)))+10*log10(nf2ff.Dmax));

drawnow

%%
disp( 'calculating 3D far field pattern and dumping to vtk (use Paraview to visualize)...' );
thetaRange = (0:2:180);
phiRange = (0:2:360) - 180;
nf2ff = CalcNF2FF(nf2ff, Sim_Path, f_res, thetaRange*pi/180, phiRange*pi/180,'Verbose',1,'Outfile','3D_Pattern.h5');

 
figure
plotFF3D(nf2ff,'logscale',-20);
 
 
E_far_normalized = nf2ff.E_norm{1} / max(nf2ff.E_norm{1}(:)) * nf2ff.Dmax;
DumpFF2VTK([Sim_Path '/3D_Pattern.vtk'],E_far_normalized,thetaRange,phiRange,'scale',1e-3);
